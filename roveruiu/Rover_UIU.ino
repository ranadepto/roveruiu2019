#include <SoftwareSerial.h>
#include <NewPing.h>
#include <LiquidCrystal.h>

int bluetoothPIN10 = 10, bluetoothPIN11 = 11;
int UltrasonicSensorPIN24 = 24;
int wheelPIN4 = 4, wheelPIN5 = 5, wheelPIN6 = 6, wheelPIN7 = 7;
int propellerPIN29 = 29, propellerPIN31 = 31, propellerPIN33 = 33, propellerPIN35 = 35;
int lcdPIN53 = 53, lcdPIN51 = 51, lcdPIN49 = 49, lcdPIN47 = 47, lcdPIN45 = 45, lcdPIN43 = 43;
int turningSignalLeftPIN8 = 8, turningSignalRightPIN9 = 9, lightsPIN2 = 2, lightingsPIN37 = 37, reverseLightsPIN3 = 3, breakPIN12 = 12;
int waterPIN22 = 22, slowPIN23 = 23, fastPIN25 = 25, buzzerPIN27 = 27, lightSensorPIN0 = 0;
int irPIN1 = 1, irPIN2 = 2, irPIN3 = 3, irPIN4 = 4;
int pingMaxDistance = 200, degree = 90, lcdColumn = 16, lcdRow = 2, waterReading, lightLevel, ir1, ir2, ir3, ir4;
boolean dualMode = false, day = true, access = false, waterMode = false, manualPropeller = false, waterFlag = true, lightFlag = false, manualLight = false, lighting = false, sensorMode = false;

SoftwareSerial BT(bluetoothPIN10, bluetoothPIN11);
NewPing sonar(UltrasonicSensorPIN24, UltrasonicSensorPIN24, pingMaxDistance); // NewPing setup of pin and maximum distance.
LiquidCrystal lcd(lcdPIN53, lcdPIN51, lcdPIN49, lcdPIN47, lcdPIN45, lcdPIN43);

String readvoice, password = "uIu";
String msg1Title = "Message UIU", msg1 = "Welcome to United International University";
String msg2Title = "Rover Details", msg2 = "Rover UIU has been Designed by Group-5 and Instructed by Ms. Zarrin Tasnim Sworna. Team members are (1) Apurbo Sarkar (2) Afsana Afrin Bonny (3) Ferdous Anam (4) Rana Depto";

void setup()
{
  BT.begin(9600);
  lcd.begin(lcdColumn, lcdRow);
  Serial.begin(9600);

  pinMode(wheelPIN4, OUTPUT);
  pinMode(wheelPIN5, OUTPUT);
  pinMode(wheelPIN6, OUTPUT);
  pinMode(wheelPIN7, OUTPUT);

  pinMode(propellerPIN29, OUTPUT);
  pinMode(propellerPIN31, OUTPUT);
  pinMode(propellerPIN33, OUTPUT);
  pinMode(propellerPIN35, OUTPUT);

  pinMode(lightsPIN2, OUTPUT);
  pinMode(lightingsPIN37, OUTPUT);
  pinMode(reverseLightsPIN3, OUTPUT);
  pinMode(breakPIN12, OUTPUT);
  pinMode(turningSignalLeftPIN8, OUTPUT);
  pinMode(turningSignalRightPIN9, OUTPUT);

  pinMode(slowPIN23, OUTPUT);
  pinMode(fastPIN25, OUTPUT);
  pinMode(buzzerPIN27, OUTPUT);

  pinMode(waterPIN22, INPUT);
  pinMode(lightSensorPIN0, INPUT);

//  pinMode(irPIN1, INPUT);
//  pinMode(irPIN2, INPUT);
//  pinMode(irPIN3, INPUT);
//  pinMode(irPIN4, INPUT);

  digitalWrite(fastPIN25, LOW);
  digitalWrite(slowPIN23, HIGH);

  Display("   Rover UIU", "-Enter Password-");
}

void forward()
{
  digitalWrite(wheelPIN4, HIGH);
  digitalWrite(wheelPIN5, HIGH);
  digitalWrite(wheelPIN6, LOW);
  digitalWrite(wheelPIN7, LOW);
  delay(100);
}

void reverse()
{
  digitalWrite(wheelPIN4, LOW);
  digitalWrite(wheelPIN5, LOW);
  digitalWrite(wheelPIN6, HIGH);
  digitalWrite(wheelPIN7, HIGH);
  delay(100);
}

void turningSignalLeft()
{
  digitalWrite(turningSignalLeftPIN8, HIGH);
}

void turningSignalRight()
{
  digitalWrite(turningSignalRightPIN9, HIGH);
}

void left()
{
  digitalWrite(wheelPIN4, HIGH);
  digitalWrite(wheelPIN5, LOW);
  digitalWrite(wheelPIN6, LOW);
  digitalWrite(wheelPIN7, HIGH);
  delay(100);
}

void right()
{
  digitalWrite(wheelPIN4, LOW);
  digitalWrite(wheelPIN5, HIGH);
  digitalWrite(wheelPIN6, HIGH);
  digitalWrite(wheelPIN7, LOW);
  delay(100);
}

void turnLeft()
{
  digitalWrite(wheelPIN4, HIGH);
  digitalWrite(wheelPIN5, LOW);
  digitalWrite(wheelPIN6, LOW);
  digitalWrite(wheelPIN7, LOW);
  delay(100);
}

void turnRight()
{
  digitalWrite(wheelPIN4, LOW);
  digitalWrite(wheelPIN5, HIGH);
  digitalWrite(wheelPIN6, LOW);
  digitalWrite(wheelPIN7, LOW);
  delay(100);
}

void boatForward()
{
  digitalWrite(propellerPIN29, HIGH);
  digitalWrite(propellerPIN31, HIGH);
  digitalWrite(propellerPIN33, LOW);
  digitalWrite(propellerPIN35, LOW);
  delay(100);
}

void boatRight()
{
  digitalWrite(propellerPIN29, HIGH);
  digitalWrite(propellerPIN31, LOW);
  digitalWrite(propellerPIN33, LOW);
  digitalWrite(propellerPIN35, LOW);
  delay(100);
}

void boatLeft()
{
  digitalWrite(propellerPIN29, LOW);
  digitalWrite(propellerPIN31, HIGH);
  digitalWrite(propellerPIN33, LOW);
  digitalWrite(propellerPIN35, LOW);
  delay(100);
}

void propeller()
{
  waterMode = true;
  Stop();
  if (manualPropeller == true)
  {
    Display("Manual Override!", " Propellers ON!");
  }
  else
  {
    Display("Water Detected!!", " Propellers ON!");
  }
  digitalWrite(lightingsPIN37, HIGH);
  boatForward();
}

void Stop()
{
  digitalWrite(wheelPIN4, LOW);
  digitalWrite(wheelPIN5, LOW);
  digitalWrite(wheelPIN6, LOW);
  digitalWrite(wheelPIN7, LOW);
  digitalWrite(propellerPIN29, LOW);
  digitalWrite(propellerPIN31, LOW);
  digitalWrite(propellerPIN33, LOW);
  digitalWrite(propellerPIN35, LOW);
  digitalWrite(reverseLightsPIN3, LOW);
  digitalWrite(lightingsPIN37, LOW);
  digitalWrite(turningSignalLeftPIN8, LOW);
  digitalWrite(turningSignalRightPIN9, LOW);
  delay(100);
}

void Break()
{
  Stop();
  digitalWrite(breakPIN12, HIGH);
  delay(5000);
  digitalWrite(breakPIN12, LOW);
  forward();
  delay(100);
}

void lights()
{
  if (day == true)
  {
    digitalWrite(lightsPIN2, HIGH);
    digitalWrite(lightingsPIN37, HIGH);
    day = false;
  }
  else
  {
    digitalWrite(lightsPIN2, LOW);
    digitalWrite(lightingsPIN37, LOW);
    digitalWrite(reverseLightsPIN3, LOW);
    day = true;
  }
  delay(20);
}

void reverseLights()
{
  digitalWrite(turningSignalLeftPIN8, LOW);
  digitalWrite(turningSignalRightPIN9, LOW);

  if (day == false && readvoice == "reverse")
    digitalWrite(reverseLightsPIN3, HIGH);
  else
    digitalWrite(reverseLightsPIN3, LOW);
  delay(100);
}

void Display(String msgLine1, String msgLine2)
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(msgLine1);
  lcd.setCursor(0, 1);
  lcd.print(msgLine2);
}

void displaySecreteMessage(String msg)
{
  lcd.clear();
  //  lcd.setCursor(0, 0);
  //  lcd.print(title);
  //  lcd.setCursor(15, 1);
  lcd.print(msg);
  for (int positionCounter = 0; positionCounter < msg.length(); positionCounter++)
  {
    lcd.scrollDisplayLeft();
    delay(250);
  }
  delay(1000);
  Display("   Job Done!", "Send New Command");
}

void message(String msg)
{
  int n = msg.length(), i, x = 0;
  double d = n / 25, m = n % 25;

  lcd.clear();
  lcd.setCursor(15, 1);
  lcd.autoscroll();
  while (d > 0)
  {
    x += 25;
    for (i = x - 25; i < x; i++)
    {
      lcd.print(msg[i]);
      delay(250);
    }
    d--;
  }

  for (i = x; i < x + m; i++)
  {
    lcd.print(msg[i]);
    delay(250);
  }

  delay(1000);
  lcd.noAutoscroll();
  lcd.clear();
  Display("   Job Done!", "Send New Command");
}

void wrongPassword()
{
  Display("Wrong Password!!", "  Try Again...");

  lcd.noDisplay();
  lights();
  digitalWrite(reverseLightsPIN3, HIGH);
  digitalWrite(breakPIN12, HIGH);
  digitalWrite(turningSignalLeftPIN8, HIGH);
  digitalWrite(turningSignalRightPIN9, HIGH);
  digitalWrite(buzzerPIN27, HIGH);
  delay(300);
  lcd.display();
  lights();
  digitalWrite(reverseLightsPIN3, LOW);
  digitalWrite(breakPIN12, LOW);
  digitalWrite(turningSignalLeftPIN8, LOW);
  digitalWrite(turningSignalRightPIN9, LOW);
  digitalWrite(buzzerPIN27, LOW);
  delay(300);
  lcd.noDisplay();
  lights();
  digitalWrite(reverseLightsPIN3, HIGH);
  digitalWrite(breakPIN12, HIGH);
  digitalWrite(turningSignalLeftPIN8, HIGH);
  digitalWrite(turningSignalRightPIN9, HIGH);
  digitalWrite(buzzerPIN27, HIGH);
  delay(300);
  lcd.display();
  lights();
  digitalWrite(reverseLightsPIN3, LOW);
  digitalWrite(breakPIN12, LOW);
  digitalWrite(turningSignalLeftPIN8, LOW);
  digitalWrite(turningSignalRightPIN9, LOW);
  digitalWrite(buzzerPIN27, LOW);
  delay(300);
  lcd.noDisplay();
  lights();
  digitalWrite(reverseLightsPIN3, HIGH);
  digitalWrite(breakPIN12, HIGH);
  digitalWrite(turningSignalLeftPIN8, HIGH);
  digitalWrite(turningSignalRightPIN9, HIGH);
  digitalWrite(buzzerPIN27, HIGH);
  delay(300);
  lcd.display();
  lights();
  digitalWrite(reverseLightsPIN3, LOW);
  digitalWrite(breakPIN12, LOW);
  digitalWrite(turningSignalLeftPIN8, LOW);
  digitalWrite(turningSignalRightPIN9, LOW);
  digitalWrite(buzzerPIN27, LOW);
}

void loop()
{
  while (BT.available())
  {
    delay(10);
    char c = BT.read();
    readvoice += c;
  }

  unsigned int distanceInCms = sonar.ping() / US_ROUNDTRIP_CM; // Send ping, get ping time in microseconds (uS) and convert it to centimeters (cm)
  //  Serial.println("Distance: " + String(distanceInCms) + "cm");
  waterReading = digitalRead(waterPIN22);
  //  Serial.println(waterReading);
  //  Serial.println();
  delay(20);

//  ir1 = analogRead(irPIN1);
//  ir2 = analogRead(irPIN2);
//  ir3 = analogRead(irPIN3);
//  ir4 = analogRead(irPIN4);
//
//  Serial.println(ir1);
//  Serial.println(ir2);
//  Serial.println(ir3);
//  Serial.println(ir4);
//  Serial.println();
//  delay(2000);

  if (access == true && waterReading == 1)
  {
    if (waterFlag == true)
    {
      propeller();
      waterFlag = false;
      sensorMode = true;
    }
  }

  else if (access == true && waterMode == true && waterReading != 1 && manualPropeller == false)
  {
    Stop();
    waterMode = false;
    waterFlag = true;
    Display("Ground Detected!", " Wheels are ON!");
    forward();
    sensorMode = true;
  }

  if (sensorMode == true && access == true && distanceInCms != NO_ECHO && distanceInCms > 15 && distanceInCms < 60)
  {
    Stop();

    if (dualMode == true)
    {
      Display("ObstacleDetected", "MovingLeft(Dual)");
      boatLeft();
      left();
      turningSignalLeft();
    }
    else if (waterMode == true)
    {
      Display("ObstacleDetected", " Turning Left");
      boatLeft();
      turningSignalLeft();
    }
    else
    {
      Display("ObstacleDetected", " Turning Right");
      right();
      turningSignalRight();
    }

    for (int i = 0; i < degree; i++)
    {
      delay(20); // Test this out and change it approriately based on your motor speed
    }

    digitalWrite(turningSignalLeftPIN8, LOW);
    digitalWrite(turningSignalRightPIN9, LOW);
    Display("Obstacle Avoided", " Moving Forward");

    if (dualMode == true)
    {
      forward();
      boatForward();

    }
    else if (waterMode == true)
    {
      boatForward();
    }
    else
    {
      forward();
    }
  }

  if (sensorMode == true && access == true && manualLight == false)
  {
    lightLevel = analogRead(lightSensorPIN0); //the sensor takes readings from analog pin A0

    if (lightLevel < 100)
    {
      if (lightFlag == false)
      {
        Display(" Dark Detected!", "   Lights ON!");
        lightFlag = true;
      }
      digitalWrite(lightsPIN2, HIGH);
      digitalWrite(lightingsPIN37, HIGH);
      day = false;
    }
 
    else
    {
      if (lightFlag == true)
      {
        Display("Lights Detected!", "   Lights OFF");
        lightFlag = false;
      }
      digitalWrite(lightsPIN2, LOW);
      digitalWrite(lightingsPIN37, LOW);
      digitalWrite(reverseLightsPIN3, LOW);
      day = true;
    }
    delay(20);

  }



  if (readvoice.length() > 0)
  {
    //    Serial.println(readvoice);

    if (access == true)
    {
      if (dualMode == true && readvoice == "forward")
      {
        Display(" Rover Command:", " Forward(Dual)");
        forward();
        boatForward();
        reverseLights();
        sensorMode = true;
      }

      else if (dualMode == true && readvoice == "reverse")
      {
        Display(" Rover Command:", " Reverse(Dual)");
        reverseLights();
        reverse();
        sensorMode = true;
      }

      else if (dualMode == true && readvoice == "right")
      {
        Display(" Rover Command:", "SharpRight(Dual)");
        reverseLights();
        turningSignalRight();
        right();
        boatRight();
        sensorMode = true;
      }

      else if (dualMode == true && readvoice == "left")
      {
        Display(" Rover Command:", "Sharp Left(Dual)");
        reverseLights();
        turningSignalLeft();
        left();
        boatLeft();
        sensorMode = true;
      }

      else if (dualMode == true && readvoice == "turn right")
      {
        Display(" Rover Command:", "Turn Right(Dual)");
        reverseLights();
        turningSignalRight();
        turnRight();
        boatRight();
        sensorMode = true;
      }

      else if (dualMode == true && readvoice == "turn left")
      {
        Display(" Rover Command:", "Turn Left(Dual)");
        reverseLights();
        turningSignalLeft();
        turnLeft();
        boatLeft();
        sensorMode = true;
      }

      else if (dualMode == true && readvoice == "break")
      {
        Display(" Rover Command:", "  Break(Dual)");
        Stop();
      }

      else if (readvoice == "forward")
      {
        Display(" Rover Command:", "    Forward");
        if (waterMode == true)
        {
          boatForward();
        }
        else
        {
          forward();
        }
        reverseLights();
        sensorMode = true;
      }

      else if (readvoice == "reverse")
      {
        if (waterMode == true)
        {
          Display("  Not Accepted", "During Boat Mode");
          digitalWrite(buzzerPIN27, HIGH);
          delay(500);
          lcd.noDisplay();
          digitalWrite(buzzerPIN27, LOW);
          delay(500);
          lcd.display();
          digitalWrite(buzzerPIN27, HIGH);
          delay(500);
          lcd.noDisplay();
          digitalWrite(buzzerPIN27, LOW);
          delay(500);
          lcd.display();
        }
        else
        {
          Display(" Rover Command:", "    Reverse");
          reverseLights();
          reverse();
        }
        sensorMode = true;
      }

      else if (readvoice == "right")
      {
        Display(" Rover Command:", "  Sharp Right");
        if (waterMode == true)
        {
          boatRight();
        }
        else
        {
          right();
        }
        reverseLights();
        turningSignalRight();
        sensorMode = true;
      }

      else if ( readvoice == "left")
      {
        Display(" Rover Command:", "   Sharp Left");
        if (waterMode == true)
        {
          boatLeft();
        }
        else
        {
          left();
        }
        reverseLights();
        turningSignalLeft();
        sensorMode = true;
      }

      else if (readvoice == "turn right")
      {
        Display(" Rover Command:", "   Turn Right");
        if (waterMode == true)
        {
          boatRight();
        }
        else
        {
          turnRight();
        }
        reverseLights();
        turningSignalRight();
        sensorMode = true;
      }

      else if ( readvoice == "turn left")
      {
        Display(" Rover Command:", "   Turn Left");
        if (waterMode == true)
        {
          boatLeft();
        }
        else
        {
          turnLeft();
        }
        reverseLights();
        turningSignalLeft();
        sensorMode = true;
      }

      else if (readvoice == "break")
      {
        Display(" Rover Command:", "     Break");
        Break();
      }

      else if (readvoice == "stop")
      {
        Display(" Rover Command:", "      Stop");
        Stop();
        digitalWrite(lightsPIN2, LOW);
        day = true;
        sensorMode = false;
      }

      else if (readvoice == "slow")
      {
        Display(" Rover Command:", "      Slow");
        digitalWrite(fastPIN25, LOW);
        digitalWrite(slowPIN23, HIGH);
      }

      else if (readvoice == "fast")
      {
        Display(" Rover Command:", "      Fast");
        digitalWrite(slowPIN23, LOW);
        digitalWrite(fastPIN25, HIGH);
      }

      else if (readvoice == "lights")
      {
        if (day == true)
        {
          Display(" Rover Command:", "   Lights ON!");
          manualLight = true;
        }
        else
        {
          Display(" Rover Command:", "   Lights OFF");
          manualLight = false;
        }
        lights();
      }

      else if (readvoice == "lighting" || readvoice == "lightings")
      {
        if (lighting == true)
        {
          digitalWrite(lightingsPIN37, LOW);
          lighting = false;
          Display(" Rover Command:", "  Lighting OFF");
          manualLight = false;
        }

        else
        {
          digitalWrite(lightingsPIN37, HIGH);
          lighting = true;
          Display(" Rover Command:", "  Lighting ON!");
          manualLight = true;
        }
      }

      else if (readvoice == "pip-pip")
      {
        Display(" Rover Command:", "    Pip-Pip!");
        digitalWrite(buzzerPIN27, HIGH);
        delay(250);
        digitalWrite(buzzerPIN27, LOW);
        delay(250);
        digitalWrite(buzzerPIN27, HIGH);
        delay(400);
        digitalWrite(buzzerPIN27, LOW);
      }

      else if (readvoice == "propeller")
      {
        sensorMode = true;
        if (manualPropeller == false)
        {
          manualPropeller = true;
          waterMode = true;
          if (waterFlag == true)
          {
            propeller();
            waterFlag = false;
          }
        }
        else
        {
          manualPropeller = false;
          waterMode = false;
          waterFlag = true;
          Display("Manual Override!", " Propellers OFF");
          Stop();
        }
      }

      else if (readvoice == msg1Title)
      {
        displaySecreteMessage(msg1);
      }

      else if (readvoice == msg2Title)
      {
        message(msg2);
      }

      else if (readvoice == "Bonny")
      {
        displaySecreteMessage("       Bonny 01823310362");
      }

      else if (readvoice == "Anam")
      {
        displaySecreteMessage("        Anam 01521105913");
      }

      else if (readvoice == "Apurbo")
      {
        displaySecreteMessage("      Apurbo 01843799960");
      }

      else if (readvoice == "Depto")
      {
        displaySecreteMessage("       Depto 01687222000");
      }

      else if (readvoice == "Dual Mode" || readvoice ==  "dual mode")
      {
        if (dualMode == false)
        {
          dualMode = true;
          Display(" Rover Command:", " Dual Mode ON!");
        }
        else
        {
          dualMode = false;
          Stop();
          Display(" Rover Command:", " Dual Mode OFF!");
        }
      }

      else
      {
        Display(" Wrong Command!", "  Try Again...");
        digitalWrite(buzzerPIN27, HIGH);
        delay(700);
        digitalWrite(buzzerPIN27, LOW);
      }

      readvoice = "";
    }

    else if (readvoice == password)
    {
      access = true;
      Display("Access Granted!!", "Hello Rover UIU!");
    }

    else
    {
      wrongPassword();
    }

    readvoice = "";
  }
}
